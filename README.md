You are given a number N. You need to find the perfect square that is nearest to it. If two perfect squares are at the same distance to N, then print the greater perfect square.

Input:
The first line of input contains T denoting the number of testcases. T testcases follow. Each testcase contains one line of input containing N.

Output:
For each testcase, in a new line, print the perfect square closest to N.